import { transformAndValidate } from "class-transformer-validator";
import {Expose, Type} from "class-transformer";
import {
  IsArray,
  IsDefined,
  IsNumber,
  IsString
} from "class-validator";
import 'reflect-metadata';


export class Album {
  @IsString()
  @Expose()
  @IsDefined()
  name!: string;
}

export class Photo {
  @IsNumber()
  @Expose()
  @IsDefined()
  id!: number;

  @IsString()
  @Expose()
  @IsDefined()
  name!: string;

  @Expose()
  @IsDefined()
  @IsArray()
  @Type(() => Album)
  albums!: Album[];
}

const object = {
  id: 5,
  name: "Juan Ramon Riquelme",
  albums: [{
    noName: "Felipe"
  }]
}

const f = async () => {
  const r = await transformAndValidate(Photo, object, {
    transformer: { excludeExtraneousValues: true },
    validator: { forbidUnknownValues: true }
  });
  console.log(r)
}

f()